import React, { Component } from "react";
import { connect } from "react-redux";

class DetailModal extends Component {
    render() {
        let { id, name, description, price, image, quantity } =
            this.props.detail;

        return (
            <div>
                <div>
                    <div
                        className="modal fade"
                        id="exampleModal"
                        tabIndex={-1}
                        role="dialog"
                        aria-labelledby="exampleModalLabel"
                        aria-hidden="true"
                    >
                        <div
                            className="modal-dialog"
                            role="document"
                            style={{ minWidth: 800 }}
                        >
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5
                                        className="modal-title"
                                        id="exampleModalLabel"
                                    >
                                        Shoe Detail
                                    </h5>
                                    <button
                                        type="button"
                                        className="close"
                                        data-dismiss="modal"
                                        aria-label="Close"
                                    >
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <div className="row">
                                        <div className="col-4">
                                            <img
                                                className="img-fluid"
                                                src={image}
                                                alt={image}
                                            />
                                        </div>
                                        <div className="col-8 text-left">
                                            <p>ID: {id}</p>
                                            <h3>{name}</h3>
                                            <p>{description}</p>
                                            <h3>$ {price}</h3>
                                            <p>Số Lượng: {quantity}</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button
                                        type="button"
                                        className="btn btn-secondary"
                                        data-dismiss="modal"
                                    >
                                        Close
                                    </button>
                                    {/* <button
                                        type="button"
                                        className="btn btn-primary"
                                    >
                                        Save changes
                                    </button> */}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

let mapStateToProps = (state) => {
    return {
        detail: state.shoeReducer.detail,
    };
};
export default connect(mapStateToProps)(DetailModal);

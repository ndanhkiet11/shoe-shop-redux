import { combineReducers } from "redux";
import { shoeReducer } from "./ShoeShopReducer";
export const rootReducer_ShoeShopRedux = combineReducers({
    shoeReducer,
});

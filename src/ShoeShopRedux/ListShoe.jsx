import React, { Component } from "react";
import { connect } from "react-redux";
import ItemShoe from "./ItemShoe";

class ListShoe extends Component {
    renderItemShoe = () => {
        return this.props.listShoe.map((item, index) => {
            return <ItemShoe data={item} key={index} />;
        });
    };

    render() {
        return <div className="row">{this.renderItemShoe()}</div>;
    }
}

let mapStateToProps = (state) => {
    return {
        listShoe: state.shoeReducer.shoeArr,
    };
};

export default connect(mapStateToProps, null)(ListShoe);

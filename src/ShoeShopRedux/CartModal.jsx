import React, { Component } from "react";
import { connect } from "react-redux";
import { CHANGE_QUANTITY } from "./redux/constants/ShoeConstant";

class CartModal extends Component {
    renderTbody = () => {
        return this.props.cart.map((item, index) => {
            return (
                <tr>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>
                        <img style={{ width: 50 }} src={item.image} alt="" />
                    </td>
                    <td>${item.price * item.number}</td>
                    <td>
                        <button
                            onClick={() => {
                                this.props.handleTangGiamSoLuong(item.id, -1);
                            }}
                            className="btn btn-light"
                        >
                            -
                        </button>
                        <span className="mx-2">{item.number}</span>
                        <button
                            onClick={() => {
                                this.props.handleTangGiamSoLuong(item.id, 1);
                            }}
                            className="btn btn-light"
                        >
                            +
                        </button>
                    </td>
                </tr>
            );
        });
    };
    render() {
        return (
            <div>
                <div
                    className="modal fade"
                    id="cartModal"
                    tabIndex={-1}
                    role="dialog"
                    aria-labelledby="cartModalLabel"
                    aria-hidden="true"
                >
                    <div
                        className="modal-dialog"
                        role="document"
                        style={{ minWidth: 850 }}
                    >
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5
                                    className="modal-title"
                                    id="exampleModalLabel"
                                >
                                    Cart
                                </h5>
                                <button
                                    type="button"
                                    className="close"
                                    data-dismiss="modal"
                                    aria-label="Close"
                                >
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <td>ID</td>
                                            <td>Name</td>
                                            <td>Image</td>
                                            <td>Price</td>
                                            <td>Quantity</td>
                                        </tr>
                                    </thead>
                                    <tbody>{this.renderTbody()}</tbody>
                                </table>
                            </div>
                            <div className="modal-footer">
                                <button
                                    onClick={this.props.handleClearCart}
                                    type="button"
                                    className="btn btn-danger"
                                >
                                    Clear Cart
                                </button>
                                <button
                                    type="button"
                                    className="btn btn-secondary"
                                    data-dismiss="modal"
                                >
                                    Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
let mapStateToProps = (state) => {
    return {
        cart: state.shoeReducer.cart,
    };
};

let mapDispatchToProps = (dispatch) => {
    return {
        handleTangGiamSoLuong: (id, num) => {
            let action = {
                type: CHANGE_QUANTITY,
                payload: { id, num },
            };
            dispatch(action);
        },
        handleClearCart: () => {
            let action = {
                type: "CLEAR_CART",
            };
            dispatch(action);
        },
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(CartModal);

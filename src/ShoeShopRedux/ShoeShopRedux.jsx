import React, { Component } from "react";
// import Cart from "./Cart";
import CartModal from "./CartModal";
import DetailModal from "./DetailModal";
// import DetailShoe from "./DetailShoe";
import ListShoe from "./ListShoe";

export default class ShoeShopRedux extends Component {
    render() {
        return (
            <div className="row container mx-auto">
                {/* <Cart /> */}
                <ListShoe />
                {/* <DetailShoe /> */}
                <DetailModal />
                <CartModal />
            </div>
        );
    }
}
